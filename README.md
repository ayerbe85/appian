**Please find below some instructions regarding Deck of Cards**

---

## Controller

You’ll find here the main class and the main method that basically consists in 4 steps

1. Initialize the deck.
2. Shuffle the deck.
3. Retrieve from the console how many cards the user wants.
4. For the given number, starting dealing one  by one the cards to the user.

---

## Service

You'll find here the Deck service and the implementation. The service has 3 methods:

1. List<Card> initDeck() - Initialize the list of cards in the specific order (like a brand new deck of cards).
2. shuffle(List<Card>) - Shuffle the list of cards
3. Card dealOneCard(List<Card> cards) - If the list  of cards is not empty, it returns the first card of the deck and removes it.

---

## DTO

You'll find here the Card class, a very simple class with just two String attributtes, value and suit.

---

*As a bonus, some Unit test are included as well.*