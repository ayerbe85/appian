package controller;

import dto.Card;
import service.DeckService;
import service.DeckServiceImpl;
import service.exceptions.EmptyDeckException;

import java.util.List;
import java.util.Scanner;

public class DeckController {
    public static void main(String[] args){
        DeckService service = new DeckServiceImpl();
        //Initialize Deck
        List<Card> cards = service.initDeck();
        //Shuffle Deck
        service.shuffle(cards);
        //Deal Cards
        System.out.println("How many cards do you want?");
        Scanner input = new Scanner(System.in);
        int num_cards = input.nextInt();
        for (int i = 0; i < num_cards ; i++){
            try {
                Card card = service.dealOneCard(cards);
                System.out.println("Your card is "+ card.getValue() + " of " + card.getSuit());
            } catch (EmptyDeckException e) {
                System.out.println("I'm sorry the deck is empty.");
                break;
            }
        }
    }
}
