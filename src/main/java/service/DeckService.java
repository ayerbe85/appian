package service;

import dto.Card;
import service.exceptions.EmptyDeckException;

import java.util.List;

public interface DeckService {
     //Method to create the Deck of Cards
     List<Card> initDeck();

     void shuffle(List<Card> cards);

     Card dealOneCard(List<Card> cards) throws EmptyDeckException;
}
