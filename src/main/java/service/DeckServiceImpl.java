package service;

import dto.Card;
import service.exceptions.EmptyDeckException;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DeckServiceImpl implements DeckService{
    private final String [] suites = {"Hearts", "Spades", "Clubs", "Diamonds"};
    private final String [] values = {"Ace","2","3","4","5","6","7","8","9","10","Jack","Queen","King"};

    @Override
    public List<Card> initDeck() {
        List<Card> deck = new ArrayList<>();
        Card card;
        for (String suit : suites){
            for (String value : values){
                card = new Card(value,suit);
                deck.add(card);
            }
        }
        return deck;
    }

    @Override
    public void shuffle(List<Card> cards) {
        Random random = new Random();
        for (int i = 0; i < cards.size(); i++){
            int pos = random.nextInt(cards.size() - i);
            Card temp = cards.get(pos);
            cards.set(pos,cards.get(i));
            cards.set(i,temp);
        }
    }

    @Override
    public Card dealOneCard(List<Card> cards) throws EmptyDeckException{
        if (cards.isEmpty()){
            //if the deck is empty, throw an error
            throw new EmptyDeckException();
        }else{
            //if the deck is not empty we return the first card
            return cards.remove(0);
        }
    }


}
