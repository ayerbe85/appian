package dto;

import org.junit.Assert;
import org.junit.Test;

public class CardTest {
    @Test
    public void CardJUTest(){
        Card c = new Card("5","spades");
        Assert.assertEquals(c.getSuit(),"spades");
        Assert.assertEquals(c.getValue(),"5");
        c.setSuit("hearts");
        c.setValue("Ace");
        Assert.assertEquals(c.getSuit(),"hearts");
        Assert.assertEquals(c.getValue(),"Ace");
    }
}
