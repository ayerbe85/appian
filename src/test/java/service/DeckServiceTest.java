package service;

import dto.Card;
import org.junit.Assert;
import org.junit.Test;
import service.exceptions.EmptyDeckException;

import java.util.ArrayList;
import java.util.List;

public class DeckServiceTest {
    private final DeckService s = new DeckServiceImpl();
    List<Card> l = new ArrayList<>();
    @Test
    public void initTest(){
        l = s.initDeck();
        Assert.assertNotNull(l);
        Assert.assertFalse(l.isEmpty());
        Assert.assertEquals(l.size(),52);
        Card firstCard = l.get(0);
        Assert.assertNotNull(firstCard);
        Assert.assertEquals(firstCard.getValue(),"Ace");
        Assert.assertEquals(firstCard.getSuit(),"Hearts");
        Card lastCard = l.get(51);
        Assert.assertNotNull(lastCard);
        Assert.assertEquals(lastCard.getValue(),"King");
        Assert.assertEquals(lastCard.getSuit(),"Diamonds");
    }

    @Test
    public  void shuffleTest(){
        l = s.initDeck();
        s.shuffle(l);
        Assert.assertNotNull(l);
        Assert.assertFalse(l.isEmpty());
        Assert.assertEquals(l.size(),52);
        Card firstCard = l.get(0);
        Assert.assertNotNull(firstCard);
        Assert.assertNotEquals(firstCard.getValue()+firstCard.getSuit(),"AceHearts");
        Card lastCard = l.get(51);
        Assert.assertNotNull(lastCard);
        Assert.assertNotEquals(lastCard.getValue()+lastCard.getSuit(),"KingDiamonds");
    }

    @Test
    public void dealCardTest() throws EmptyDeckException {
        l = s.initDeck();
        s.shuffle(l);
        int originalSize = l.size();
        Card firstCard = s.dealOneCard(l);
        Assert.assertNotNull(l);
        Assert.assertEquals(l.size(),originalSize-1);
        Card secondCard = s.dealOneCard(l);
        Assert.assertNotNull(l);
        Assert.assertEquals(l.size(),originalSize-2);
        Assert.assertNotEquals(firstCard.getValue() + firstCard.getSuit()
                ,secondCard.getValue() + secondCard.getSuit());
    }

    @Test(expected = EmptyDeckException.class)
    public void emptyDeckExceptionTest() throws EmptyDeckException {
        l = s.initDeck();
        s.shuffle(l);
        Assert.assertEquals(l.size(),52);
        while (l.size()>0){
            s.dealOneCard(l);
        }
        //Throw Exception
        s.dealOneCard(l);
    }
}
